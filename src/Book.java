/**
 * Created by Надиа on 18.03.2017.
 */
public class Book extends Literature {


    String author;
    String publisher;


    public Book(String title, String publisher, String author, int year) {
        super(title, year);
        this.publisher = publisher;
        this.author = author;
    }


    @Override
    public String getAll() {
        return super.getAll() + author + publisher;
    }


}
