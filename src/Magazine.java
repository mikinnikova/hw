/**
 * Created by Надиа on 18.03.2017.
 */
public class Magazine extends Literature {
    String theme;


    public Magazine(String title, String theme, int year) {
        super(title, year);
        this.theme = theme;
    }

    @Override
    public String getAll() {
        return super.getAll() + theme;
    }
}
