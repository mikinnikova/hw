/**
 * Created by Надиа on 21.03.17.
 */
public class Libruary {
    public Literature[] literatures;


    public Libruary() {
        this.literatures = generateLitreture();


    }


    public Literature[] generateLitreture() {
        Literature[] literates = new Literature[9];


        literates[0] = new Dictionary(" Dictionary1 ", " Theme1 ", " Publisher1 ", 2001);
        literates[1] = new Dictionary(" Dictionary2 ", " Theme2 ", " Publisher2 ", 2009);
        literates[2] = new Dictionary(" Dictionary3 ", " Theme3 ", " Publisher3 ", 2016);
        literates[3] = new Magazine(" Magazine1 ", " Theme1 ", 2011);
        literates[4] = new Magazine(" Magazine2 ", " Theme2 ", 2014);
        literates[5] = new Magazine(" Magazine3 ", " Theme3 ", 2013);
        literates[6] = new Book(" Book1 ", " Publisher1 ", " Anknown1 ", 2000);
        literates[7] = new Book(" Book2 ", " Publisher2 ", " Anknown2 ", 2015);
        literates[8] = new Book(" Book3 ", " Publisher3 ", " Anknown3 ", 2010);

        return literates;
    }


    public void printLiterature3() {
        int year = 2017;
        System.out.println("Литература за последние три года: ");
        for (Literature literature : literatures) {
            if (literature.year > (year - 3))
                System.out.println(literature.getAll());

        }
    }

    public void printLiterature5() {
        int year = 2017;
        System.out.println("Литература за последние 5 лет: ");
        for (Literature literature : literatures) {
            if (literature.year > (year - 5))
                System.out.println(literature.getAll());

        }
    }

    public void printLiterature10() {
        int year = 2017;
        System.out.println("Литература за последние 10 лет: ");
        for (Literature literature : literatures) {
            if (literature.year > (year - 10))
                System.out.println(literature.getAll());

        }
    }
}
