/**
 * Created by Надиа on 18.03.2017.
 */
public class Dictionary extends Literature {
    String theme;
    String publisher;

    public Dictionary(String title, String theme, String publisher, int year) {
        super(title, year);
        this.theme = theme;
        this.publisher = publisher;
    }

    @Override
    public String getAll() {
        return super.getAll() + theme + publisher;
    }

}

